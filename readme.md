# Many Amigos - Web Developer Test

----

## Installation

### 1. Clone it

To install, first clone a copy of the project into your file system:

`git clone git@bitbucket.org:k7n4n5t3w4rt/web-developer-test.git`

### 2. Get Dependencies

- Go to the document root
- Run `composer install`
- Run `npm install`

----

## Laravel set up

Documentation on setting up Laravel can be found here - http://laravel.com/docs/installation

### 1. Development envoronment

Set up your local development environment, eg: http://laravel.com/docs/quick#local-development-environment

NOTE: The "development" environment settings are configured to expect
`manyamigos.dev` as a local development domain

### 2. MySQL Database

Make a MySQL database. The default database configuration (app/config/database.php) is:

            'database'  => 'manyamigos_cms',
            'username'  => 'root',
            'password'  => '',

Run the `artisan` migration to populate the database (http://laravel.com/docs/migrations#running-migrations)

php artisan migrate


----

## Front End

### 1. Building

To build front end into `public` directory, use `gulp` (`npm install -g gulp`).


----

## Testing

To setup Codeception on server:

    wget http://chromedriver.storage.googleapis.com/2.10/chromedriver_linux64.zip
    unzip chromedriver_linux64.zip
    sudo mv chromedriver /usr/bin/chromedriver

To test, run `vendor/bin/codecept run --steps`
