@extends('layouts.public')
@section('content')
    {{ Form::open(array('url' => '#', 'method' => 'post','id'=>'contactusform')) }} 
    {{ Form::label('lblFirstName', 'First Name'); }}
    {{ Form::text('txtFirstname','',array('required' => 'true','id'=>'fname')); }} 
    {{ Form::label('lblSurname', 'Surname'); }} 
    {{ Form::text('txtSurname','',array('required' => 'true')); }} 
    {{ Form::label('lbl=Email', 'Email Address'); }} 
    {{ Form::text('txtEmail','',array('required' => 'true')); }}
    {{ Form::label('lblDaytimeContact', 'Daytime contact number'); }}
    {{ Form::text('txtDayTimeContact','',array('required' => 'true')); }} 
    {{ Form::label('lblAddress', 'Address'); }}
    {{ Form::text('txtAddress','',array('required' => 'true')); }} 
    {{ Form::label('lblSuburb', 'Suburb'); }} 
    {{ Form::text('txtSuburb','',array('required' => 'true')); }} 
    {{ Form::label('lblState', 'State'); }}
    {{ Form::select('selState', array('' => '-- Select --', 'act' => 'ACT', 'nsw' => 'NSW','nt'=>'NT','qld'=>'QLD','sa'=>'SA','tas'=>'TAS','vic'=>'VIC','wa'=>'WA'),'',array('required' => 'true')); }} 
    {{ Form::label('lblPostCode', 'Post Code'); }}
    {{ Form::text('txtPostCode','',array('required' => 'true')); }}
    {{ Form::label('lblEnquiryType', 'Enquiry Type'); }}
    {{ Form::select('selEnquiryType', array('' => '-- Select --','genEn' => 'General enquiry', 'feedback' => 'Feedback or Enquiry','proComplaint'=>'Product complaint'),'',array('required' => 'true')); }}
    {{  Form::label('lblProductName', 'Product Name'); }}
    {{ Form::text('txtProductName','',array('required' => 'true')); }} 
    @if ($errors->has('txtProductName')) <p class="help-block">Product name is required</p> @endif
    {{  Form::label('lblProductSize', 'Product Size'); }}
    {{ Form::text('txtProductSize','',array('required' => 'true')); }} 
    @if ($errors->has('txtProductSize')) <p class="help-block">Product size is required</p> @endif
    {{  Form::label('lblUseByDate', 'Use-by date'); }}
    {{ Form::text('txtUseByDate','',array('required' => 'true')); }} 
    @if ($errors->has('txtUseByDate')) <p class="help-block">Use by date is required</p> @endif
    {{  Form::label('lblBatchCode', 'Batch Code'); }}
    {{ Form::text('txtBatchCode','',array('required' => 'true')); }} 
    @if ($errors->has('txtBatchCode')) <p class="help-block">Batch Code is required</p> @endif
    {{ Form::label('lblEnquiry', 'Enquiry'); }}
    {{ Form::textarea('Enquiry'); }} 
    {{ Form::submit('Submit',array('id'=>'submitBtn','name'=>'submit')); }} 
    {{ Form::close() }}
@stop