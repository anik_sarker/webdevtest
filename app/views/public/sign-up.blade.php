@extends('layouts.public')

@section('content')

    <div class="row">
        <div class="medium-5 columns">
            <ul class="pricing-table">
                <li class="title">Get Started</li>
                <li class="price"><small>FREE during beta</small></li>
                <li class="description">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                    nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                </li>
                <li class="bullet-item">1 Year Subscription</li>
                <li class="bullet-item">Priority Support</li>
                <li class="bullet-item">Lorem Ipsum</li>
                <li class="cta-button">
                    <a class="button" href="#">Forward Yourself</a>
                </li>
            </ul>
        </div>
    </div>

@stop