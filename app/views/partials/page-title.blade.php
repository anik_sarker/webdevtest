<div class="page-title @if (isset($pageTitleClass)){{ $pageTitleClass }}@endif">
    <div class="row">
        <h2>{{ $pageTitle }}</h2>
        <hr>
    </div>
</div>