<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

        // Roles
        $this->call('RoleTableSeeder');
        $this->command->info('Role table seeded!');

        // Users
		$this->call('UserTableSeeder');
        $this->command->info('User table seeded!');
	}

}

class RoleTableSeeder extends Seeder {

    public function run()
    {
        DB::table('user_roles')->delete();
        DB::table('roles')->delete();
        Role::create(array('name' => 'administrator'));
        Role::create(array('name' => 'author'));
    }
}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        // Standard test admin user
        $user = User::create(array(
            'username'   => 'adminAmigo',
            'password'   => Hash::make('S3cur34cc355!'),
            'email'      => 'ManyAmigosAWS@gmail.com',
            'first_name' => 'Admin',
            'last_name'  => 'User'
        ));
        $user->addRole('administrator');
        $user->addRole('author');

        // Standard test author user
        $user = User::create(array(
            'username'   => 'authorAmigo',
            'password'   => Hash::make('S3cur34cc355!'),
            'email'      => 'ManyAmigosAWS@gmail.com',
            'first_name' => 'Author',
            'last_name'  => 'User'
        ));
        $user->addRole('author');
    }

}

