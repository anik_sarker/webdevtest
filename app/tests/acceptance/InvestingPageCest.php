<?php
use \WebGuy;

class InvestingPageCest
{
    /**
     * Checks that the content in the How it Works: Investing page is present
     *
     * @param WebGuy $I
     */
    public function checkContent(WebGuy $I)
    {
        $I->wantTo('check that the content is present in page');
        $I->amOnpage('/how-it-works/investing');
        $I->see('Investing');
    }

    /**
     * Checks that the main menu element is in the DOM
     *
     * @param WebGuy $I
     */
    public function checkMainMenuIsInDOM(WebGuy $I)
    {
        $I->wantTo('ensure menu is present in DOM');
        $I->amOnpage('/how-it-works/investing');
        $I->seeElementInDOM('#main-menu');
    }

    /**
     * Check for Google Analytics on page
     *
     * @param WebGuy $I
     */
    public function checkForGoogleAnalyticsInDOM(WebGuy $I)
    {
        $I->wantTo('check that Google analytics is present on page');
        $I->amOnPage('/how-it-works/investing');
        $I->seeInPageSource('UA-52164821-1');
    }

}