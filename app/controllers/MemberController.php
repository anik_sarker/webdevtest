<?php

class MemberController extends BaseController
{

    /**
     * Members List
     *
     * @return mixed
     */
    public function showMembersList()
    {
        $viewItems = array(
            'pageTitle' => 'Members'
        );
        return View::make('members.list', $viewItems);
    }

    /**
     * Member Profile
     *
     * @param $code
     * @return mixed
     */
    public function showMember($code)
    {
        $viewItems = array(
            'code' => $code
        );
        return View::make('members.member', $viewItems);
    }

}
