<?php

class UserController extends BaseController
{

    /**
     * Shows the sign in page
     *
     * @return mixed
     */
    public function showAdministratorSignInPage() {
        $viewItems = array(
            'pageTitle'    => 'Administration Sign In',
            'formOptions'  => array(
                'method' => 'post',
                'name'   => 'ma-sign-in-form'
            )
        );
        return View::make('public.admin-sign-in', $viewItems);
    }


    /**
     * Shows the sign up page
     *
     * @return mixed
     */
    public function showSignUpPage() {
        $viewItems = array(
            'pageTitle' => 'Sign Up'
        );
        return View::make('public.sign-up', $viewItems);
    }

    /**
     * Signs in an administrator (CMS user)
     *
     * On success, redirects the user to their intended destination
     *  (with a fallback to /ma-admin), otherwise on failure, redirects
     *  back to administrator sign-in page with an error message
     */
    public function signInAdministrator() {
        // If required params aren't present, exit out immediately
        if (!Input::has('username') || !Input::has('password')) {
            return Redirect::to('ma-sign-in')
                ->with('errorMessage', 'Username and password fields are required');
        }

        // Attempt to authenticate user
        if (Auth::attempt(array(
            'username' => Input::get('username'),
            'password' => Input::get('password')
        ))) {
            // Check for permissions
            if (Auth::user()->hasRole('administrator')) {
                return Redirect::intended('ma-admin');
            } else {
                Auth::logout();
            }
        }

        // If we got this far we failed to login
        return Redirect::to('ma-sign-in')
            ->with('errorMessage', 'Username or password incorrect');
    }

    /**
     * Signs out and administrator (CMS user)
     */
    public function signOutAdministrator() {
        // Logout user
        Auth::logout();

        return Redirect::to('ma-sign-in')
            ->with('signOutMessage', 'You have signed out of the Site Administration');
    }

    /**
     * Signs a user in
     */
    public function signInUser() {

    }

    /**
     * Signs a user out
     */
    public function signOutUser() {

    }

    /**
     * Signs a user up
     */
    public function signUpUser() {

    }

} 