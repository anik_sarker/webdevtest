<?php

return array(

    /**
     * Settings page title
     *
     * @type string
     */
    'title' => 'How it Works',

    /**
     * The edit fields array
     *
     * @type array
     */
    'edit_fields' => array(
        'heading' => array(
            'title' => 'Heading1',
            'type'  => 'text',
            'limit' => 100,
        )
    ),

    /**
     * The validation rules for the form, based on the Laravel validation class
     *
     * @type array
     */
    'rules' => array(
        'heading' => 'required|max:100'
    ),

    /**
     * This is run prior to the JSON file being saved
     *
     * @type function
     * @param array $data
     *
     * @return string {on error} / void {otherwise}
     */
    'before_save' => function(&$ata)
    {

    },

    /**
     * Authentication check specific for this settings page
     */
    'permissions' => function()
    {
        return (Auth::check() && Auth::user()->hasRole('administrator'));
    },

    /**
     * Custom actions for settings page
     */
    'actions' => array(

    )

);