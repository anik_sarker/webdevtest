(function(angular) {
    angular.module('manyAmigosClient')
        .controller('MainMenuController', ['$scope', '$window', function($scope, $window) {
            // Let's tap into the server side routing and find out if we're on
            // the home page. If we aren't, let's appear the menu
            $scope.hideMenu = ($window.location.pathname === '/');
        }]);
})(angular);