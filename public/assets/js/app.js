'use strict';
(function(angular, jQuery) {
    angular.module('manyAmigosClient', [])
        .config([function() {
        }])
        .run(function() {
            jQuery(document).foundation();
        });
})(angular, jQuery);